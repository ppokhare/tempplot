#!/bin/bash
#SBATCH --account=def-mdanning
#SBATCH --mem=32000M # memory per node
#SBATCH --time=0-10:0
#SBATCH --cpus-per-task=28 # CPU cores/threads
#SBATCH --output=/home/ppokhare/projects/ctb-stelzer/ppokhare/temp/Jupyter/slurm-notebook-%j.%x.out

module load python/3.8.10 cuda/11.1.1 scalapack
source activate /home/ppokhare/gnn4/bin/activate
module load scipy-stack

unset XDG_RUNTIME_DIR
jupyter-lab --ip $(hostname -f) --no-browser
